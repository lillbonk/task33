import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
  
})

export class AuthService {

  constructor(private http: HttpClient) { }

  register(user): Promise<any>{

    return this.http.post(`${environment.apiUrl}/v1/api/users/register`,
      {
      user: { ...user },
    }).toPromise();

  }

/**
 * attempt to login with given user
 * @param user 
 * @returns boolean
 * 
 */
  login(user: any): Promise<any> {
    return this.http.post(`${environment.apiUrl}/v1/api/users/login`, 
    {
      user: {...user},

  }).toPromise()

   }

}



