import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginPageComponent } from './components/pages/login-page/login-page.component';
import { RegisterPageComponent } from './components/pages/register-page/register-page.component';
import { AuthGuard } from './guards/auth.guard';


const routes: Routes = [
  {
    path: 'login',
    component: LoginPageComponent
  },
  {
    path: 'register',
    component: RegisterPageComponent
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./components/pages/dashboard/dashboard.module').then(m => m.DashboardModule),
    canActivate: [AuthGuard ]
  },
  {
    path: 'surveys',
    loadChildren: () => import('./components/survey/survey-list/survey-list.module').then(m => m.SurveyListModule),
    canActivate: [ AuthGuard ]
  },
  {
    path: 'surveys/:id',
    loadChildren: () => import('./components/survey/survey-detail/survey-detail.module').then(m => m.SurveyDetailModule),
    canActivate: [ AuthGuard ]
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/login'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
