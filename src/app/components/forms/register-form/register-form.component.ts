import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { AuthService } from 'src/app/services/auth/auth.service';
import { SessionService } from 'src/app/services/session/session.service';


@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit{

  registerForm: FormGroup = new FormGroup({
    username: new FormControl('',
    [ Validators.required,
      Validators.minLength(5),
      Validators.email
    ]),
    password: new FormControl('' , 
    [ Validators.required,
      Validators.pattern('^(?=.*?[A-Z)(?=.*?[a-z])(?=.*?[0-9]).{6,}$')
    ]),
    confirmPassword: new FormControl('', 
    [Validators.required,
      Validators.pattern('^(?=.*?[A-Z)(?=.*?[a-z])(?=.*?[0-9]).{6,}$')
    ])
  })


  constructor(private authservice: AuthService , private router: Router, private session: SessionService){

    if (this.session.get() !== false) {
      this.router.navigateByUrl('/dashboard');
    }

  }

  get username() {
    return this.registerForm.get('username');
  };
  get password() {
    return this.registerForm.get('password');
  };
  get confirmPassword() {
    return this.registerForm.get('confirmPassword');
  };

  isLoading: boolean = false;
  registerError: string;
  


 async onRegisterClick() {

  this.registerError = "";


  try {
    this.isLoading = true;
  

    if (this.password.value !== this.confirmPassword.value){
      return alert('Passwords do not match. Try again.')
    }
    // exec the register fucntion from the service
   
    const result: any = await this.authservice.register(this.registerForm.value);

    if (result.status < 400) {
      this.session.save({
        token: result.data.token,
        username: result.data.user.username
      });
      this.router.navigateByUrl('/dashboard');
    }

  } catch (e) {

    this.registerError = e.error.error;
    

  } finally{
    this.isLoading = false;
  }
  
    
  }

  ngOnInit(): void {
  }  


}
