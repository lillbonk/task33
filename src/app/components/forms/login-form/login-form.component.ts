import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { AuthService } from 'src/app/services/auth/auth.service';
import { SessionService } from 'src/app/services/session/session.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent  implements OnInit {

loginForm: FormGroup = new FormGroup({
  username: new FormControl('', 
  [ Validators.required,
    Validators.minLength(5),
    Validators.email
  ]),
  password: new FormControl('', 
  [ Validators.required,
    Validators.minLength(6)

  ])

})

constructor(private authService: AuthService, private router: Router, private session: SessionService) {

  if (this.session.get() !== false) {
    this.router.navigateByUrl('/dashboard');
  }
 }

get username() {
  return this.loginForm.get('username')
}
get password() {
  return this.loginForm.get('password')
}

isLoading: boolean = false;
loginError: string;

ngOnInit(): void {
}

 async onLoginClick(){

  this.loginError = '';

  try {
    this.isLoading = true;
    const result: any = await this.authService.login(this.loginForm.value);

    if (result.status < 400){
      this.session.save ({ 
        token: result.data.token,
        username: result.data.user.username
      });
      this.router.navigateByUrl('/dashboard');
    }
    
  } catch (e) {
    this.loginError = e.error.error;
  } finally {
    this.isLoading = false;
  }



      
      
  }

}

